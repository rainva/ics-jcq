package com.ics.tongdb.jcq;

/**
 * @author lisiqun
 * @version V1.0
 * @Description:
 * @Company:G'YUN NETWORK TECHNOLOGY
 * @date 2020-04-13 11:41
 */
public enum  SocketAction {
    BIND,
    MESSAGE,
    NAME_CHANGE,
    CLOSE_WINDOWS,
    RECEIVE_REPLY;

    private SocketAction() {
    }
}
