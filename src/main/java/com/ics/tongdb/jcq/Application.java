package com.ics.tongdb.jcq;

import com.alibaba.fastjson.JSON;
import com.sobte.cqp.jcq.entity.Anonymous;
import com.sobte.cqp.jcq.entity.CQDebug;
import com.sobte.cqp.jcq.entity.CQImage;
import com.sobte.cqp.jcq.entity.Group;
import com.sobte.cqp.jcq.entity.GroupFile;
import com.sobte.cqp.jcq.entity.ICQVer;
import com.sobte.cqp.jcq.entity.IMsg;
import com.sobte.cqp.jcq.entity.IRequest;
import com.sobte.cqp.jcq.event.JcqAppAbstract;
import java.awt.Component;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JOptionPane;

/**
 * @author lisiqun
 * @version V1.0
 * @Description:
 * @Company:G'YUN NETWORK TECHNOLOGY
 * @date 2020-04-13 11:37
 */
public class Application  extends JcqAppAbstract implements ICQVer, IMsg, IRequest {
    private Timer timer = new Timer();
    private Connection conn;
    public Application() {
    }

    public String appInfo() {
        String AppID = "com.ics.tongdb.jcq.application";
        return "9," + AppID;
    }

    public int startup() {
        SocketClient.userQq = CQ.getLoginQQ() + "";
        SocketClient.getInstance();

        try {
            this.conn = SqlLiteUtils.createConnection();
            System.out.println("sqlite已连接");
        } catch (SQLException var4) {
            var4.printStackTrace();
            System.out.println("sqlite连接异常");
        }

        this.timer.schedule(new TimerTask() {
            public void run() {
                Application.this.pushGroupName();
            }
        }, 0L, 3600000L);
        this.timer.schedule(new TimerTask() {
            public void run() {
                SqlLiteUtils.valid(Application.this.conn);
            }
        }, 0L, 15000L);
        String cmd = " powercfg -x -standby-timeout-ac 0 && powercfg -x -standby-timeout-dc 0 && powercfg -x -disk-timeout-ac 0 && powercfg -x -disk-timeout-dc 0 && powercfg -x -hibernate-timeout-ac 0 && powercfg -x -hibernate-timeout-dc 0";

        try {
            Runtime.getRuntime().exec("cmd.exe /c " + cmd);
        } catch (IOException var3) {
            var3.printStackTrace();
        }

        return 0;
    }

    public int exit() {
        SocketClient.getInstance().exit();
        this.timer.cancel();
        if (this.conn != null) {
            try {
                this.conn.close();
                System.out.println("sqlite关闭");
            } catch (SQLException ex) {
                ex.printStackTrace();
                System.out.println("sqlite关闭异常");
            }
        }

        return 0;
    }

    public int enable() {
        enable = true;
        return 0;
    }

    public int disable() {
        enable = false;
        return 0;
    }

    public void downloadUsingNIO(String urlStr, String file) throws IOException {
        URL url = new URL(urlStr);
        ReadableByteChannel rbc = Channels.newChannel(url.openStream());
        FileOutputStream fos = new FileOutputStream(file);
        fos.getChannel().transferFrom(rbc, 0L, 9223372036854775807L);
        fos.close();
        rbc.close();
    }

    public int privateMsg(int subType, int msgId, long fromQQ, String msg, int font) {
        SocketMessage model = new SocketMessage(SocketAction.MESSAGE.toString(), "1.0.4");
        List<CQImage> imageList = CC.getCQImages(msg);
        Map<String, String> photoList = model.getPhotoList();
        if (imageList != null && imageList.size() > 0) {
            Iterator var10 = imageList.iterator();

            while(var10.hasNext()) {
                CQImage cqImage = (CQImage)var10.next();
                photoList.put(cqImage.getMd5(), cqImage.getUrl());
            }
        }

        model.setCtime(new Date());
        model.setType(3);
        model.setGroup((String)null);
        model.setDiscuss((String)null);
        model.setGroupName((String)null);
        model.setQq(fromQQ + "");
        model.setQqNick(CQ.getStrangerInfo(fromQQ).getNick());
        model.setUserQq(CQ.getLoginQQ() + "");
        model.setMsg(msg);
        SocketClient.getInstance().send(model);
        SqlLiteUtils.updateState(this.conn, String.valueOf(msgId));
        return 0;
    }

    public int groupMsg(int subType, int msgId, long fromGroup, long fromQQ, String fromAnonymous, String msg, int font) {
        if (fromQQ == 80000000L && !fromAnonymous.equals("")) {
            Anonymous anonymous = CQ.getAnonymous(fromAnonymous);
            return 0;
        } else {
            SocketMessage model = new SocketMessage(SocketAction.MESSAGE.toString(), "1.0.4");
            List<CQImage> imageList = CC.getCQImages(msg);
            Map<String, String> photoList = model.getPhotoList();
            if (imageList != null && imageList.size() > 0) {
                Iterator var13 = imageList.iterator();

                while(var13.hasNext()) {
                    CQImage cqImage = (CQImage)var13.next();
                    photoList.put(cqImage.getMd5(), cqImage.getUrl());
                }
            }

            List<Group> list = CQ.getGroupList();
            String name = null;
            Iterator var15 = list.iterator();

            while(var15.hasNext()) {
                Group group = (Group)var15.next();
                long id = group.getId();
                if (id == fromGroup) {
                    name = group.getName();
                }
            }

            model.setGroupName(name);
            model.setCtime(new Date());
            model.setType(1);
            model.setDiscuss((String)null);
            model.setGroup(fromGroup + "");
            model.setUserQq(CQ.getLoginQQ() + "");
            model.setQq(fromQQ + "");
            model.setQqNick(CQ.getStrangerInfo(fromQQ).getNick());
            model.setMsg(msg);
            SocketClient.getInstance().send(model);
            SqlLiteUtils.updateState(this.conn, String.valueOf(msgId));
            return 0;
        }
    }

    private void pushGroupName() {
        List<Group> list = CQ.getGroupList();
        String msg = JSON.toJSONString(list);
        SocketMessage model = new SocketMessage(SocketAction.NAME_CHANGE.toString(), "1.0.4");
        model.setCtime(new Date());
        model.setMsg(msg);
        SocketClient.getInstance().send(model);
    }

    public int discussMsg(int subtype, int msgId, long fromDiscuss, long fromQQ, String msg, int font) {
        SocketMessage model = new SocketMessage(SocketAction.MESSAGE.toString(), "1.0.4");
        List<CQImage> imageList = CC.getCQImages(msg);
        Map<String, String> photoList = model.getPhotoList();
        if (imageList != null && imageList.size() > 0) {
            Iterator var12 = imageList.iterator();

            while(var12.hasNext()) {
                CQImage cqImage = (CQImage)var12.next();
                photoList.put(cqImage.getMd5(), cqImage.getUrl());
            }
        }

        List<Group> list = CQ.getGroupList();
        String name = null;
        Iterator var14 = list.iterator();

        while(var14.hasNext()) {
            Group group = (Group)var14.next();
            long id = group.getId();
            if (id == fromDiscuss) {
                name = group.getName();
            }
        }

        model.setGroupName(name);
        model.setCtime(new Date());
        model.setType(2);
        model.setGroup((String)null);
        model.setDiscuss(fromDiscuss + "");
        model.setUserQq(CQ.getLoginQQ() + "");
        model.setQq(fromQQ + "");
        System.out.println("msgId" + msgId);
        model.setMsg(msg);
        model.setQqNick(CQ.getStrangerInfo(fromQQ).getNick());
        SocketClient.getInstance().send(model);
        SqlLiteUtils.updateState(this.conn, String.valueOf(msgId));
        return 0;
    }

    public int groupUpload(int subType, int sendTime, long fromGroup, long fromQQ, String file) {
        GroupFile groupFile = CQ.getGroupFile(file);
        return groupFile == null ? 0 : 0;
    }

    public int groupAdmin(int subtype, int sendTime, long fromGroup, long beingOperateQQ) {
        return 0;
    }

    public int groupMemberDecrease(int subtype, int sendTime, long fromGroup, long fromQQ, long beingOperateQQ) {
        return 0;
    }

    public int groupMemberIncrease(int subtype, int sendTime, long fromGroup, long fromQQ, long beingOperateQQ) {
        return 0;
    }

    public int friendAdd(int subtype, int sendTime, long fromQQ) {
        return 0;
    }

    public int requestAddFriend(int subtype, int sendTime, long fromQQ, String msg, String responseFlag) {
        return 0;
    }

    public int requestAddGroup(int subtype, int sendTime, long fromGroup, long fromQQ, String msg, String responseFlag) {
        return 0;
    }

    public int menuA() {
        JOptionPane.showMessageDialog((Component)null, "这是测试菜单A，可以在这里加载窗口");
        return 0;
    }

    public int menuB() {
        JOptionPane.showMessageDialog((Component)null, "这是测试菜单B，可以在这里加载窗口");
        return 0;
    }

    public static void init() {
        CQ = new CQDebug();
        CQ.logInfo("[JCQ] TEST Demo", "测试启动");
    }
}
