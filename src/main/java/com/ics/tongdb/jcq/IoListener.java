package com.ics.tongdb.jcq;

import org.apache.mina.core.service.IoService;
import org.apache.mina.core.service.IoServiceListener;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

/**
 * @author lisiqun
 * @version V1.0
 * @Description:
 * @Company:G'YUN NETWORK TECHNOLOGY
 * @date 2020-04-13 11:38
 */
public class IoListener implements IoServiceListener {
    public IoListener() {
    }

    public void serviceActivated(IoService arg0) throws Exception {
    }

    public void serviceDeactivated(IoService arg0) throws Exception {
    }

    public void serviceIdle(IoService arg0, IdleStatus arg1) throws Exception {
    }

    public void sessionCreated(IoSession arg0) throws Exception {
    }

    public void sessionClosed(IoSession ioSession) throws Exception {
    }

    public void sessionDestroyed(IoSession arg0) throws Exception {
    }

}
