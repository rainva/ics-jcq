package com.ics.tongdb.jcq;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lisiqun
 * @version V1.0
 * @Description:
 * @Company:G'YUN NETWORK TECHNOLOGY
 * @date 2020-04-13 11:31
 */
public class SocketMessage {
    private String id;
    private String action;
    private String version;
    private Integer type;
    private String group;
    private String discuss;
    private String groupName;
    private String qq;
    private String qqNick;
    private String userQq;
    private String msg;
    private Map<String, String> photoList;
    private String ip;
    private Integer replyState;
    private Date ctime;

    public SocketMessage() {
    }

    public SocketMessage(String action, String version) {
        this.action = action;
        this.version = version;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAction() {
        return this.action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getGroup() {
        return this.group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getDiscuss() {
        return this.discuss;
    }

    public void setDiscuss(String discuss) {
        this.discuss = discuss;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getQq() {
        return this.qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getQqNick() {
        return this.qqNick;
    }

    public void setQqNick(String qqNick) {
        this.qqNick = qqNick;
    }

    public String getUserQq() {
        return this.userQq;
    }

    public void setUserQq(String userQq) {
        this.userQq = userQq;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Map<String, String> getPhotoList() {
        if (this.photoList == null) {
            this.photoList = new HashMap();
        }

        return this.photoList;
    }

    public void setPhotoList(Map<String, String> photoList) {
        this.photoList = photoList;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getReplyState() {
        return this.replyState;
    }

    public void setReplyState(Integer replyState) {
        this.replyState = replyState;
    }

    public Date getCtime() {
        return this.ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

}
