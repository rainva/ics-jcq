package com.ics.tongdb.jcq;

import java.awt.Component;
import java.awt.Desktop;
import java.net.URI;
import javax.swing.JOptionPane;

/**
 * @author lisiqun
 * @version V1.0
 * @Description:
 * @Company:G'YUN NETWORK TECHNOLOGY
 * @date 2020-04-13 11:35
 */
public class StringUtils {
    public StringUtils() {
    }

    public static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }

    public static void main(String[] args) {
        getJOptionPane("http://xxxx.com");
    }

    public static void getJOptionPane(String url) {
        int n = JOptionPane.showConfirmDialog((Component)null, "官方做了新的变动，如果不更新将无法正常使用", "版本更新", 0);
        if (n == 0) {
            try {
                Desktop.getDesktop().browse(new URI(url));
            } catch (Exception var3) {
                var3.printStackTrace();
            }
        }

    }
}
