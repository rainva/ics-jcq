package com.ics.tongdb.jcq;

import com.alibaba.fastjson.JSON;
import com.sobte.cqp.jcq.entity.CQImage;
import com.sobte.cqp.jcq.entity.Group;
import com.sobte.cqp.jcq.event.JcqApp;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author lisiqun
 * @version V1.0
 * @Description:
 * @Company:G'YUN NETWORK TECHNOLOGY
 * @date 2020-04-13 11:35
 */
public class SqlLiteUtils {
    private static final String Class_Name = "org.sqlite.JDBC";
    private static final String DB_URL;

    public SqlLiteUtils() {
    }

    public static void main(String[] args) {
        Application.init();
    }

    public static Connection createConnection() throws SQLException {
        try {
            Class.forName("org.sqlite.JDBC");
            System.out.println("加载驱动");
        } catch (ClassNotFoundException var1) {
            System.out.println("SqlLite数据库驱动未找到!");
        }

        Connection conn = DriverManager.getConnection(DB_URL, (String)null, (String)null);
        conn.setAutoCommit(false);
        return conn;
    }

    public static void valid(Connection connection) {
        ResultSet rs = null;
        PreparedStatement pstmt = null;

        try {
            Statement statement = connection.createStatement();
            rs = statement.executeQuery("select * FROM event where extra != 1 and type != 2001 order by time asc;");
            pstmt = connection.prepareStatement("update event SET extra = 1 where id =?");
            reSent(rs, pstmt);
            connection.commit();
        } catch (SQLException var16) {
            var16.printStackTrace();
            System.err.println(var16.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException var15) {
                    var15.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException var14) {
                    var14.printStackTrace();
                }
            }

        }

    }

    public static void reSent(ResultSet rs, PreparedStatement pstmt) throws SQLException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(14, -5);
        long time2 = calendar.getTimeInMillis();

        while(true) {
            while(true) {
                String group;
                String account;
                String content;
                do {
                    do {
                        do {
                            do {
                                if (!rs.next()) {
                                    pstmt.executeBatch();
                                    return;
                                }

                                group = rs.getString("group");
                                account = rs.getString("account");
                            } while("".equals(account));

                            content = rs.getString("content");
                        } while("".equals(content));
                    } while(content.startsWith("[CQ:rich"));
                } while(content.startsWith("[CQ:image"));

                content = formatContent(content);
                long fromQQ = Long.parseLong(account.replace("qq/user/", ""));
                long time = rs.getLong("time");
                Date date = new Date(time * 1000L);
                List<CQImage> imageList = Application.CC.getCQImages(content);
                Map<String, String> photoList = new HashMap();
                if (imageList != null && imageList.size() > 0) {
                    Iterator var15 = imageList.iterator();

                    while(var15.hasNext()) {
                        CQImage cqImage = (CQImage)var15.next();
                        photoList.put(cqImage.getMd5(), cqImage.getUrl());
                    }
                }

                if ("".equals(group)) {
                    SocketMessage model = new SocketMessage(SocketAction.MESSAGE.toString(), "1.0.4");
                    model.setCtime(date);
                    model.setType(3);
                    model.setGroup((String)null);
                    model.setGroupName((String)null);
                    model.setQq(fromQQ + "");
                    model.setQqNick(JcqApp.CQ.getStrangerInfo(fromQQ).getNick());
                    model.setUserQq(JcqApp.CQ.getLoginQQ() + "");
                    model.setMsg(content);
                    model.setPhotoList(photoList);
                    if (time < time2) {
                        System.out.println("send前qq-model" + JSON.toJSONString(model));
                        SocketClient.getInstance().send(model);
                        pstmt.setString(1, rs.getString("id"));
                        pstmt.addBatch();
                    }
                } else {
                    String fromGroup = group.replace("qq/group/", "");
                    SocketMessage model = new SocketMessage(SocketAction.MESSAGE.toString(), "1.0.4");
                    List<Group> groups = JcqApp.CQ.getGroupList();
                    String name = null;
                    Iterator var19 = groups.iterator();

                    while(var19.hasNext()) {
                        Group temp = (Group)var19.next();
                        String id = temp.getId() + "";
                        if (id.equals(fromGroup)) {
                            name = temp.getName();
                        }
                    }

                    model.setGroupName(name);
                    model.setCtime(date);
                    model.setType(1);
                    model.setGroup(fromGroup + "");
                    model.setUserQq(JcqApp.CQ.getLoginQQ() + "");
                    model.setQq(fromQQ + "");
                    model.setQqNick(JcqApp.CQ.getStrangerInfo(fromQQ).getNick());
                    model.setMsg(content);
                    model.setPhotoList(photoList);
                    if (time < time2) {
                        System.out.println("send前group-model" + JSON.toJSONString(model));
                        SocketClient.getInstance().send(model);
                        pstmt.setString(1, rs.getString("id"));
                        pstmt.addBatch();
                    }
                }
            }
        }
    }

    private static String formatContent(String content) {
        String result = content;
        Pattern p = Pattern.compile("(?<=\\[)(\\S+)(?=\\])");

        for(Matcher m = p.matcher(content); m.find(); result = result.replace("[" + m.group(1) + "] ", "")) {
        }

        return result;
    }

    public static void updateState(Connection connection, String msgId) {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate("update event SET extra = 1 where id =" + msgId + ";");
            connection.commit();
            System.out.println("更新extra:" + msgId);
        } catch (SQLException var3) {
            var3.printStackTrace();
            System.err.println(var3.getMessage());
            System.out.println("更新extra失败:" + msgId);
        }

    }

    static {
        DB_URL = "jdbc:sqlite:" + System.getProperty("user.dir").replace("\\", "\\\\") + "\\\\data\\\\" + JcqApp.CQ.getLoginQQ() + "\\\\eventv2.db";
    }
}
