package com.ics.tongdb.jcq;

/**
 * @author lisiqun
 * @version V1.0
 * @Description:
 * @Company:G'YUN NETWORK TECHNOLOGY
 * @date 2020-04-13 11:32
 */
public class Version {
    public static final String VALUE = "1.0.4";

    public Version() {
    }
}
